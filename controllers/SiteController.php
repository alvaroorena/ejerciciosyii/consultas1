<?php

namespace app\controllers;

use Yii;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
    public function actionConsulta1dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT DISTINCT edad FROM ciclista',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 1 con DAO",
           "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
           "sql"=>"SELECT DISTINCT edad FROM ciclista"
       ]);
        
        
    }
    
    
    public function actionConsulta1orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("edad")
                ->distinct(),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 1 con ORM",
           "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
           "sql"=>"SELECT DISTINCT edad FROM ciclista"
       ]);
        
        
        
        
        
    }
    
    public function actionConsulta2dao(){
        
      
        
       $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = "Artiach"',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 2 con DAO",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach",
           "sql"=>'SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = "Artiach"'
       ]);
        
        
    }
    
    public function actionConsulta2orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 2 con ORM",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach",
           "sql"=>'SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = "Artiach"'
       ]);
        
        
        
        
        
    }
    
    
     public function actionConsulta3dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = "Artiach" OR c.nomequipo= "Amore Vita" ',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 3 con DAO",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
           "sql"=>'SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = "Artiach" OR c.nomequipo= "Amore Vita"'
       ]);
        
        
    }
    
    public function actionConsulta3orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach' OR nomequipo='Amore Vita'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 3 con ORM",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
           "sql"=>'SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = "Artiach" OR c.nomequipo= "Amore Vita"'
       ]);
        
        
        
        
        
    }
    
    public function actionConsulta4dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.edad<25 OR  c.edad>30',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 4 con DAO",
           "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
           "sql"=>'SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.edad<25 OR c.edad>30'
       ]);
        
        
    }
    
    public function actionConsulta4orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("dorsal")
                ->distinct()
                ->where("edad<25 OR edad>30"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 4 con ORM",
           "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
           "sql"=>'SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.edad<25 OR c.edad>30'
       ]);
        
        
        
        
    }
    
    
    public function actionConsulta5dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>' SELECT c.dorsal FROM  ciclista c WHERE c.edad BETWEEN 28 AND 32 AND c.nomequipo="Banesto"',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 5 con DAO",
           "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
           "sql"=>' SELECT c.dorsal FROM  ciclista c WHERE c.edad BETWEEN 28 AND 32 AND c.nomequipo="Banesto";
'
       ]);
        
        
    }
    
    public function actionConsulta5orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("dorsal")
                ->distinct()
                ->where("edad BETWEEN 28 AND 32 AND nomequipo='Banesto'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 5 con ORM",
           "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
           "sql"=>' SELECT c.dorsal FROM  ciclista c WHERE c.edad BETWEEN 28 AND 32 AND c.nomequipo="Banesto";
'
       ]);
        
        
        
        
    }
    
    
    
    
    
    public function actionConsulta6dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>' SELECT c.nombre FROM ciclista c WHERE CHAR_LENGTH(c.nombre)>8',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre'],
           "titulo"=>"Consulta 6 con DAO",
           "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
           "sql"=>' SELECT c.nombre FROM ciclista c WHERE CHAR_LENGTH(c.nombre)>8;
'
       ]);
        
        
    }
    
    
    public function actionConsulta6orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("nombre")
                ->distinct()
                ->where("CHAR_LENGTH(nombre)>8"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre'],
           "titulo"=>"Consulta 6 con ORM",
           "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
           "sql"=>' SELECT c.nombre FROM ciclista c WHERE CHAR_LENGTH(c.nombre)>8;
'
       ]);
        
        
        
        
        
    }
    
    public function actionConsulta7dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT UPPER(nombre) AS "nombre mayúsculas",c.dorsal FROM ciclista c',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre mayúsculas','dorsal'],
           "titulo"=>"Consulta 7 con DAO",
           "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
           "sql"=>' SELECT UPPER(nombre) AS "nombre mayúsculas",c.dorsal FROM ciclista c;
'
       ]);
        
        
    }
    
    public function actionConsulta7orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Ciclista::find()
                ->select("UPPER(nombre)AS nombremayúsculas, dorsal")
                ->distinct(),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombremayúsculas','dorsal'],
           "titulo"=>"Consulta 7 con ORM",
           "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
           "sql"=>' SELECT UPPER(nombre) AS "nombre mayúsculas",c.dorsal FROM ciclista c;
'
       ]);
        
        
        
        
        
    }
    
    
    public function actionConsulta8dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>' SELECT DISTINCT dorsal FROM lleva l WHERE l.código="MGE"',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 8 con DAO",
           "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
           "sql"=>' SELECT DISTINCT dorsal FROM lleva l WHERE l.código="MGE";
'
       ]);
        
        
    }
    
    public function actionConsulta8orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Lleva::find()
                ->select("dorsal ")
                ->distinct()
                ->where("código='MGE'"),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 8 con ORM",
           "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
           "sql"=>'SELECT DISTINCT dorsal FROM lleva l WHERE l.código="MGE";
'
       ]);
        
        
        
        
        
    }
    
    
    
    public function actionConsulta9dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>' SELECT p.nompuerto FROM puerto p WHERE p.altura>1500',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nompuerto'],
           "titulo"=>"Consulta 9 con DAO",
           "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
           "sql"=>'SELECT p.nompuerto FROM puerto p WHERE p.altura>1500;
'
       ]);
        
        
    }
    
    public function actionConsulta9orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Puerto::find()
                ->select("nompuerto")
                ->distinct()
                ->where("altura>1500"),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nompuerto'],
           "titulo"=>"Consulta 9 con ORM",
           "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
           "sql"=>'SELECT p.nompuerto FROM puerto p WHERE p.altura>1500;
'
       ]);
        
        
         
    }
    
    
    
    public function actionConsulta10dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT DISTINCT p.dorsal FROM  puerto p  WHERE p.pendiente>8 OR p.altura BETWEEN 1800 AND 3000',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 10 con DAO",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
           "sql"=>' SELECT DISTINCT p.dorsal FROM  puerto p  WHERE p.pendiente>8 OR p.altura BETWEEN 1800 AND 3000;
'
       ]);
        
        
    }
    
    public function actionConsulta10orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Puerto::find()
                ->select("dorsal")
                ->distinct()
                ->where("pendiente>8 OR altura BETWEEN 1800 AND 3000"),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
      return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 10 con ORM",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
           "sql"=>' SELECT DISTINCT p.dorsal FROM  puerto p  WHERE p.pendiente>8 OR p.altura BETWEEN 1800 AND 3000;
'
       ]);
        
        
         
    }
    
    public function actionConsulta11dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT p.dorsal FROM puerto p WHERE p.pendiente>8 AND p.altura BETWEEN 1800 AND 3000',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 11 con DAO",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
           "sql"=>' SELECT p.dorsal FROM puerto p WHERE p.pendiente>8 AND p.altura BETWEEN 1800 AND 3000;
'
       ]);
        
        
    }
    
    public function actionConsulta11orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Puerto::find()
                ->select("dorsal")
                ->distinct()
                ->where("pendiente>8 AND altura BETWEEN 1800 AND 3000"),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
      return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 11 con ORM",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
           "sql"=>' SELECT DISTINCT p.dorsal FROM  puerto p  WHERE p.pendiente>8 AND p.altura BETWEEN 1800 AND 3000;
'
       ]);
    
    
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
